*** Settings ***
Documentation     A test suite for APIs Integration test
Library     RequestsLibrary

Resource    ../Resources/APIs/Session.robot
Resource    ../Resources/APIs/Countries.robot
Resource    ../Resources/APIs/Industries.robot
Resource    ../Resources/APIs/Companies.robot
Resource    ../Resources/APIs/Currencies.robot
Resource    ../Resources/APIs/Portfolios.robot
Resource    ../Resources/APIs/ValueChain.robot

Suite Setup             Create Session    finvarplatform    https://api-dev.finvar.net      verify=True    disable_warnings=0

*** Variables ***
${BASE_DEV_URL}         https://api-dev.finvar.net
${valid_email}          vassol@gmail.com
${valid_password}       12345678
            

***Keywords***


*** Test Cases ***
Login to the Finvar Platform with valid credentials
    [Documentation]    Login to Finvar Platform - APIs
    [Tags]             Smoke Test           Authentication
    ${HEAD1}           Create Dictionary    Authorization=    Content-Type=application/json
    ${response} =      Session.Authenticate to the Platform with Bearer Token

Verify authenticated user information
    [Documentation]    User info in Finvar Platform - APIs
    [Tags]             Smoke Test           Authentication
    ${response} =      Session.Get currently authenticated user information        ${header2}

Verify all countries available
    [Documentation]    Verifying Countries - APIs
    [Tags]             Smoke Test       Countries
    Countries.Get all countries     

Verify all industries available
    [Documentation]    Verifying Industries - APIs
    [Tags]             Smoke Test       Industries
    Industries.Get all industries  

Searching a Company
    [Documentation]    Searching Companies - APIs
    [Tags]             Smoke Test       Companies
    Companies.Search Companies by keyword   

Verify all Companies available
    [Documentation]    Searching Companies - APIs
    [Tags]             Smoke Test       Companies
    Companies.Get all Companies  

Verify if an existing user can view all the currencies
    [Documentation]    Verifying Currencies - APIs
    [Tags]             Smoke Test       Currencies
    Currencies.Get all currencies      

Get Portfolios from the user logged
    [Documentation]    Verifying Portfolios - APIs
    [Tags]             Smoke Test       Portfolios
    Portfolios.Get all portfolios           

Verify if the user logged can create a new Portfolio
    [Documentation]    Creating Portfolios - APIs
    [Tags]             Smoke Test       Portfolios
    Portfolios.Create a new portfolio  

Verify if the Portfolio created is available
    [Documentation]    Verifying Portfolios - APIs
    [Tags]             Smoke Test       Portfolios
    Portfolios.Get Portfolio by id      

Verify if the Portfolio created can be deleted
    [Documentation]    Verifying Portfolios - APIs
    [Tags]             Smoke Test       Portfolios
    Portfolios.Delete Portfolio by id  

Verify if the Value Chain
    [Documentation]    Verifying Value chain - APIs
    [Tags]             Smoke Test       Portfolios
    ValueChain.Get all Value Chain by Org ID  