*** Settings ***
Documentation  Test Suite for FINVAR Platform
Library           DataDriver    file=AutomationTEST.xlsx    sheet_name=PORTFOLIO
Library     SeleniumLibrary
# notice we're no longer referencing the Selenium2Library in our script!
#Resource  ../Resources/search_resources.robot
Resource     ../Resources/FinvarApp.robot
Resource     ../Resources/Common.robot  # necessary for Setup & Teardown

Test Setup  Begin Web Test
Test Teardown  End Web Test

Test Template   Portfolios


*** Variables ***
${BROWSER} =  chrome
${START_URL} =  https://web-dev.finvar.net/main/
${HOMEPAGE_TITLE} =  Finvar


# Copy/paste the below line to Terminal window to execute
# robot -d results tests/Rentalo.robot

*** Test Cases ***
Portfolios      ${scenario}	${searchitem}	${input_email}	${input_pass}	${link}	${email}	${password}	${searchitem2}	${portfolioId}	${link2}	${link3}	${portfolioName}	${PortfolioAccess}	${count}	${portfolioSummary}	${portfolioCompany}	${portfolioRelationship}	${portfolioTypeOfProduct}	${SizeRelationship}	${currency}
    

*** Keywords ***
Portfolios
    [Arguments]   ${scenario}	${searchitem}	${input_email}	${input_pass}	${link}	${email}	${password}	${searchitem2}	${portfolioId}	${link2}	${link3}	${portfolioName}	${PortfolioAccess}	${count}	${portfolioSummary}	${portfolioCompany}	${portfolioRelationship}	${portfolioTypeOfProduct}	${SizeRelationship}	${currency}
    FinvarApp.Verify Portfolios Finvar Platform      ${scenario}	${searchitem}	${input_email}	${input_pass}	${link}	${email}	${password}	${searchitem2}	${portfolioId}	${link2}	${link3}	${portfolioName}	${PortfolioAccess}	${count}	${portfolioSummary}	${portfolioCompany}	${portfolioRelationship}	${portfolioTypeOfProduct}	${SizeRelationship}	${currency}

    