*** Settings ***
Documentation  Test Suite for FINVAR Platform
Library           DataDriver    file=AutomationTEST.xlsx    sheet_name=SESSION
Library     SeleniumLibrary
# notice we're no longer referencing the Selenium2Library in our script!
#Resource  ../Resources/search_resources.robot
Resource     ../Resources/FinvarApp.robot
Resource     ../Resources/Common.robot  # necessary for Setup & Teardown

Test Setup  Begin Web Test
Test Teardown  End Web Test

Test Template   Log In


*** Variables ***
${BROWSER} =  chrome
${START_URL} =  https://web-dev.finvar.net/main/
${HOMEPAGE_TITLE} =  Finvar


# Copy/paste the below line to Terminal window to execute
# robot -d results tests/Rentalo.robot

*** Test Cases ***

Log In      ${scenario}	${searchitem}	${input_email}	${input_pass}	${link}	${email}	${password}	${searchitem2}	${link2}
    

*** Keywords ***
Log In
    [Arguments]   ${scenario}	${searchitem}	${input_email}	${input_pass}	${link}	${email}	${password}	${searchitem2}	${link2}
    FinvarApp.Login Finvar Platform      ${scenario}	${searchitem}	${input_email}	${input_pass}	${link}	${email}	${password}	${searchitem2}	${link2}

    