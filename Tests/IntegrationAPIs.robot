*** Settings ***
Documentation     A test suite for APIs Integration test
Library     RequestsLibrary

Resource    ../Resources/APIs/Session.robot
Resource    ../Resources/APIs/Countries.robot
Resource    ../Resources/APIs/Industries.robot
Resource    ../Resources/APIs/Companies.robot
Resource    ../Resources/APIs/Currencies.robot
Resource    ../Resources/APIs/Portfolios.robot
Resource    ../Resources/APIs/ValueChain.robot

Suite Setup             Create Session    finvarplatform    https://api-dev.finvar.net      verify=True    disable_warnings=0

*** Variables ***
${BASE_DEV_URL}         https://api-dev.finvar.net
${valid_email}          vassol@gmail.com
${valid_password}       12345678
            

***Keywords***


*** Test Cases ***
#Login to the Finvar Platform with valid credentials
#    ${HEAD1}           Create Dictionary   Authorization=    Content-Type=application/json
#    ${response} =         Session.Authenticate to the Platform with Bearer Token  
#    #${header2}         Create Dictionary   Authorization=    Content-Type=application/json
#    ${token}            set variable        ${response.json()["token"]}
#    ${bearerToken}      set variable        Bearer ${token}
#    ${head}=            Create Dictionary   Authorization=${bearerToken}    Content-Type=application/json
#    ${user_id}          Set Variable        ${response.json()["user"]["id"]}
#    #Set Suite Variable      ${header2}       ${HEAD1}  
#    Set Suite Variable      ${header2}       ${head}  

Login to the Finvar Platform with valid credentials
    [Documentation]    Login to Finvar Platform - APIs
    [Tags]             Smoke Test           Authentication
    ${HEAD1}           Create Dictionary    Authorization=    Content-Type=application/json
    ${response} =      Session.Authenticate to the Platform with Bearer Token

Verify all users available
    [Documentation]    Users available in Finvar Platform - APIs
    [Tags]             Smoke Test           Authentication
    ${response} =      Session.Get all users available      ${header2}

Verify authenticated user information
    [Documentation]    User info in Finvar Platform - APIs
    [Tags]             Smoke Test           Authentication
    ${response} =      Session.Get currently authenticated user information        ${header2}

Verify all countries available
    [Documentation]    Verifying Countries - APIs
    [Tags]             Smoke Test       Countries
    Countries.Get all countries     

Verify all industries available
    [Documentation]    Verifying Industries - APIs
    [Tags]             Smoke Test       Industries
    Industries.Get all industries  

Searching a Company
    [Documentation]    Searching Companies - APIs
    [Tags]             Smoke Test       Companies
    Companies.Search Companies by keyword   

Verify all Companies available
    [Documentation]    Searching Companies - APIs
    [Tags]             Smoke Test       Companies
    Companies.Get all Companies  

Verify if an existing user can view all the currencies
    [Documentation]    Verifying Currencies - APIs
    [Tags]             Smoke Test       Currencies
    Currencies.Get all currencies      ${header2}


Get Portfolios from the user logged
    [Documentation]    Verifying Portfolios - APIs
    [Tags]             Smoke Test       Portfolios
    Portfolios.Get all portfolios           ${header2}

Verify if the user logged can create a new Portfolio
    [Documentation]    Creating Portfolios - APIs
    [Tags]             Smoke Test       Portfolios
    #${types}=          Portfolios.Get Portfolio User Types     ${header2}       ${user_id}
    #Portfolios.Create a new portfolio       ${header2}

Verify if the Portfolio created is available
    [Documentation]    Verifying Portfolios - APIs
    [Tags]             Smoke Test       Portfolios
    #${types}=          Portfolios.Get Portfolio User Types     ${header2}       ${user_id}
    Portfolios.Get Portfolio by id      ${header2}

Verify if the Portfolio created can be deleted
    [Documentation]    Verifying Portfolios - APIs
    [Tags]             Smoke Test       Portfolios
    #${types}=          Portfolios.Get Portfolio User Types     ${header2}       ${user_id}
    Portfolios.Delete Portfolio by id  

Verify if the Value Chain
    [Documentation]    Verifying Value chain - APIs
    [Tags]             Smoke Test       Portfolios
    #${types}=          Portfolios.Get Portfolio User Types     ${header2}       ${user_id}
    ValueChain.Get all Value Chain by Org ID  