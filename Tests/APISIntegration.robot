*** Settings ***
Library    String
Library    REST          https://api-dev.finvar.net/api/v1/        ssl_verify=false
Set expectations
    Expect response   [
  {
    "id": "93a2fb6e-e977-4683-bd3c-4e0b93c9f255",
    "name": "France",
    "code": null,
    "population": 50000000
  },
  {
    "id": "bb918d2e-0bae-42c0-9c09-49998c521728",
    "name": "Mexico",
    "code": null,
    "population": 0
  },
  {
    "id": "dbf54d89-e9cf-449e-b7f6-49eba0b84f96",
    "name": "United States",
    "code": null,
    "population": 0
  },
  {
    "id": "50ae68ff-4b60-4355-a26b-8ff91ccd81a6",
    "name": "United Kingdom",
    "code": null,
    "population": 0
  }
]
    Expect response   { "seconds": { "maximum": 5} }
*** Test Cases ***
Get Employee
  GET       countries
  Output    response  body
  #String    response body employee      1001