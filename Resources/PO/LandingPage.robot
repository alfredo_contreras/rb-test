*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${HOMEPAGE_GENERAL} =  The Next Generation Platform for Financial Professionals
#${HOMEPAGE_GENERAL} =  The Next Generation Platform for Financial Professionals
${LISTA} =   portfolios
${portfolios_link_nav_menu} =   //*[@id="root"]/div/div/div[1]/div[1]/div/nav/div/div/div/div/div/p[1]
${features_link_nav_menu} =     //*[@id="root"]/div/div/div[1]/div[1]/div/nav/div/button/h6
${features_portfolio_link} =    //*[@id="root"]/div/div/div[1]/div[1]/div/nav/div/div/div/div/div/p[1]
${features_section} =           //*[@id="root"]/div/div/div[2]/div[2]/div/div
${features_portfolio_text} =    Create and share multiple asset portfolios
${features_data_link} =         //*[@id="root"]/div/div/div[2]/div[2]/div/div/div[1]/button[2]
${features_data_text} =         Create and share multiple asset portfolios

*** Keywords ***
Load Website
    Go To  ${START_URL}
    Maximize Browser Window 

Verify Website loaded
    Wait Until Page Contains    ${HOMEPAGE_TITLE}
    Capture page Screenshot

Verify General Section
    Wait Until Page Contains    ${HOMEPAGE_GENERAL}
    Capture page Screenshot

Verify Features Menu available
    Wait Until page Contains Element    xpath=${features_link_nav_menu} 
    Click Element       xpath=${features_link_nav_menu}

LogIn
    Input Text      name=${input_email2}    ${email2}
    Input Text      name=${input_pass2}     ${password2}
    Click button    xpath=//*[@id="root"]/div/div/div[1]/div[1]/div/form/div[3]/button
    Wait Until Page Contains    ${searchitemNew2}
    Capture Page Screenshot

Goto Portfolio Page
    Wait Until Page Contains Element   xpath=//a[@href="${link2}"] 
    Click Link      xpath=//a[@href="${link2}"]
    Wait Until Page Contains    PORTFOLIOS
    Capture Page Screenshot

Goto Portfolio Features section
    Wait Until Page Contains Element    xpath=${features_portfolio_link}
    #Click button    xpath=//*[@id="root"]/div/div/div[1]/div[1]/div/form/div[3]/button
    Click Element                       xpath=${features_portfolio_link}
    Capture Page Screenshot

Goto Data & Analysis Features section
    Wait Until Page Contains Element    ${features_data_link}
    Click Element                        xpath=${features_data_link}
    Wait Until Page Contains            ${features_data_text}
    Capture Page Screenshot