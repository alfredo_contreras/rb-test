*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${LISTA} =   portfolios
${PORTFOLIO_TITLE} =        PORTFOLIOS
${ADD_PORTFOLIO_TITLE} =    Add Portfolio
${portfolios_link_nav_menu} =   //*[@id="root"]/div/div/div[1]/div[1]/div/nav/div/div/div/div/div/p[1]
${add_portfolio} =              //*[@id="add-icon"]


*** Keywords ***
Create new Portfolio
    Wait Until Page Contains Element    ${add_portfolio}
    Click Element                       xpath=${add_portfolio}      
    Wait Until Page Contains            ${ADD_PORTFOLIO_TITLE}
    Capture Page Screenshot

Verify Portfolio created

Modify Portfolio created

Delete Portfolio created


Verify Website loaded
    Wait Until Page Contains    ${HOMEPAGE_TITLE}

LogIn
    Input Text      name=${input_email2}    ${email2}
    Input Text      name=${input_pass2}     ${password2}
    Click button    xpath=//*[@id="root"]/div/div/div[1]/div[1]/div/form/div[3]/button
    Wait Until Page Contains    ${searchitemNew2}
    Capture Page Screenshot

Goto Portfolio Page
    Wait Until Page Contains Element   xpath=//a[@href="${link2}"] 
    Click Link      xpath=//a[@href="${link2}"]
    Wait Until Page Contains    PORTFOLIOS
    Capture Page Screenshot