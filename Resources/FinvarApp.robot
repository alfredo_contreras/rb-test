*** Settings ***
Resource    ../Resources/Common.robot  # necessary for Setup & Teardown
Resource    ../Resources/PO/LandingPage.robot
Resource    ../Resources/PO/PortfolioPage.robot
Resource    ../Resources/search_resources.robot


*** Keywords ***
Search for features
    LandingPage.Load Website
    LandingPage.Verify Website loaded
    
Login Finvar Platform
    [Arguments]   ${scenario}	${searchitem}	${input_email}	${input_pass}	${link}	${email}	${password}	${searchitem2}	${link2}
    Set Suite Variable      ${email2}           ${email} 
    Set Suite Variable      ${password2}        ${password} 
    Set Suite Variable      ${input_email2}     ${input_email} 
    Set Suite Variable      ${input_pass2}      ${input_pass} 
    Set Suite Variable      ${searchitemNew1}   ${searchitem} 
    Set Suite Variable      ${searchitemNew2}   ${searchitem2} 
    Run Keyword If      '${scenario}'=='LOGIN1'
    ...     Run Keywords        
    ...         Log into Finvar Platform    
    

# Escenarios de Pruebas Positivos con datos validos
Log into Finvar Platform 
    LandingPage.Load Website
    LandingPage.Verify Website loaded
    LandingPage.LogIn


Verify Portfolios Finvar Platform
    [Arguments]   ${scenario}	${searchitem}	${input_email}	${input_pass}	${link}	${email}	${password}	${searchitem2}	${portfolioId}	${link2}	${link3}	${portfolioName}	${PortfolioAccess}	${count}	${portfolioSummary}	${portfolioCompany}	${portfolioRelationship}	${portfolioTypeOfProduct}	${SizeRelationship}	${currency}

    Set Suite Variable      ${email2}           ${email} 
    Set Suite Variable      ${password2}        ${password} 
    Set Suite Variable      ${input_email2}     ${input_email} 
    Set Suite Variable      ${input_pass2}      ${input_pass} 
    Set Suite Variable      ${searchitemNew1}   ${searchitem} 
    Set Suite Variable      ${searchitemNew2}   ${searchitem2} 
    Set Suite Variable      ${portfolioId2}     ${portfolioId}
    Set Suite Variable      ${link2}                ${link2}
    Set Suite Variable      ${link3}                ${link3}
    Set Suite Variable      ${link3}                ${link3}
    Set Suite Variable      ${portfolioName2}       ${portfolioName}
    Set Suite Variable      ${PortfolioAccess2}     ${PortfolioAccess}
    Set Suite Variable      ${count2}               ${count}
    Set Suite Variable      ${portfolioSummary2}    ${portfolioSummary}
    Set Suite Variable      ${portfolioCompany2}    ${portfolioCompany}
    Set Suite Variable      ${portfolioRelationship2}       ${portfolioRelationship}
    Set Suite Variable      ${portfolioTypeOfProduct2}      ${portfolioTypeOfProduct}
    Set Suite Variable      ${currency2}            ${currency}
    Set Suite Variable      ${SizeRelationship2}    ${SizeRelationship}
    Run Keyword If      '${scenario}'=='PORTFOLIO1'
    ...     Run Keywords        
    ...         Verify Portfolios into Finvar Platform
    ...         PortfolioPage.Create new Portfolio
    ...         PortfolioPage.Verify Portfolio created
    ...         PortfolioPage.Modify Portfolio created
    ...         PortfolioPage.Delete Portfolio created

    

# Escenarios de Pruebas Positivos con datos validos
Verify Portfolios into Finvar Platform 
    LandingPage.Load Website
    LandingPage.Verify Website loaded
    LandingPage.LogIn
    LandingPage.Goto Portfolio Page