*** Settings ***
Documentation     Countries API available on the Finvar Platform
Library     RequestsLibrary

*** Variables ***
${search_keyword}       PEPSI

*** Keywords ***
Get all Companies
    [Documentation]     Organizations Apis integration test
    ${response}=        Get On Session     finvarPlatform        /api/v1/organizations/       headers=${header2}
    Status Should Be    200                 ${response}    

Search Companies by keyword
    [Documentation]     Organizations Apis integration test
    ${response}=        Get On Session     finvarPlatform        /api/v1/organizations/search/${search_keyword}       headers=${header2}
    Status Should Be    200                 ${response}    
    