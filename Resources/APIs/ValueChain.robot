*** Settings ***
Documentation     Countries API available on the Finvar Platform
Library     RequestsLibrary

*** Variables ***
${orgId}       c340cc49-77fe-4b50-bed1-1d5aeb34e93a

*** Keywords ***
Get all Value Chain by Org ID
    [Documentation]     Value Chain by Organizations Apis integration test
    ${response}=        Get On Session     finvarPlatform        /api/v1/value-chains/${orgId}       headers=${header2}
    Status Should Be    200                 ${response}    
 
    