*** Settings ***
Documentation     Portfolios API available on the Finvar Platform
Library     RequestsLibrary

*** Variables ***
${name}             EDN-VIII 
${description}      Portfolio Description VIII
${summary}          Portfolio EDN-VIII 
${type}             Public 
${tag1}             TRANSPORTATION3
${tag2}             OIL3
           

*** Keywords ***
Get all portfolios
    [Documentation]     Portfolios Apis integration test
    ${response}=        Get On Session     finvarPlatform        /api/v1/portfolios       headers=${header2}
    Status Should Be    200                 ${response}    


Create a new portfolio
    [Documentation]     Portfolios Apis integration test
    @{tags}=            Create list         ${tag1}     ${tag2}
    ${types}=           Get Portfolio User Types            
    ${members}=         Create Dictionary   id=${user_id}       portfolio_user_type=${types}
    @{list_member}=     Create list         ${members}
    #@{members}=         Create Dictionary   id=${user_id}       portfolio_user_type=${types}
    &{data}=            Create Dictionary   name=${name}        description=${description}      summary=${summary}      type=${type}    tags=${tags}    members=${list_member}
    ${response}=        Post On Session     finvarPlatform        /api/v1/portfolios/       headers=${header2}    json=${data} 
    ${jsondata}=        set variable        ${response.json()}    
    ${portfolio_id}=    set variable        ${jsondata['id']}
    Set Suite Variable      ${portfolioId}  ${portfolio_id}  
    Status Should Be    201                 ${response}    


Get Portfolio User Types
    ${response}=        Get On Session      finvarPlatform      /api/v1/portfolio-users-types/  headers=${header2}
    ${jsondata}=        set variable        ${response.json()}
    ${json2}=           set variable        ${jsondata}
    ${user_type}=       set variable        ${json2[0]['id']}
    Status Should Be    200                 ${response}
    Return From Keyword           ${user_type}


Get Portfolio by id
    ${response}=        Get On Session      finvarPlatform      /api/v1/portfolios/${portfolio_id}      headers=${header2}   
    ${jsondata}=        set variable        ${response.json()}
    ${json2}=           set variable        ${jsondata}
    Status Should Be    200                 ${response}
    #Return From Keyword           ${user_type}

Delete Portfolio by id
    ${response}=        Delete On Session      finvarPlatform      /api/v1/portfolios/${portfolio_id}      headers=${header2}    
    ${jsondata}=        set variable        ${response.json()}
    ${json2}=           set variable        ${jsondata}
    Status Should Be    200                 ${response}
    #Return From Keyword           ${user_type}