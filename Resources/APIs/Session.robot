*** Settings ***
Documentation     Auth Apis available on the Finvar Platform
Library     RequestsLibrary

*** Variables ***
${valid_email}          vassol@gmail.com
${not_valid_email}      vas23sol@gmail.com
${valid_password}       12345678
${not_valid_password}   56465465


*** Keywords ***
Authenticate to the Platform with valid credentials
    &{data}=    Create Dictionary    email=${valid_email}    password=${valid_password}
    ${response}=        Post On Session     finvarPlatform        /api/v1/auth/login       json=${data}
    Should Be Equal As Strings    ${response.status_code}    200


Authenticate to the Platform with Bearer Token - NOT USED
    &{data}=            Create Dictionary   email=${valid_email}    password=${valid_password}
    ${response}=        Post On Session     finvarPlatform        /api/v1/auth/login       json=${data}
    ${token}            set variable        ${response.json()["token"]}
    ${bearerToken}      set variable        Bearer ${token}
    ${head}=            Create Dictionary   Authorization=${bearerToken}    Content-Type=application/json
    ${head1}=           Set variable        Authorization=${bearerToken}    Content-Type=application/json
    ${user_id}          Set Variable        ${response.json()["user"]["id"]}
    Status Should Be    200                 ${response}    
    #Return From Keyword           ${head}
    Return From Keyword           ${response}

Authenticate to the Platform with Bearer Token
    &{data}=            Create Dictionary   email=${valid_email}    password=${valid_password}
    ${response}=        Post On Session     finvarPlatform        /api/v1/auth/login       json=${data}
    ${token}            set variable        ${response.json()["token"]}
    ${bearerToken}      set variable        Bearer ${token}
    ${head}=            Create Dictionary   Authorization=${bearerToken}    Content-Type=application/json
    ${head1}=           Set variable        Authorization=${bearerToken}    Content-Type=application/json
    ${user_id}          Set Variable        ${response.json()["user"]["id"]}
    Status Should Be    200                 ${response}    
    Set Suite Variable      ${header2}       ${head}  
    Set Suite Variable      ${userid}        ${user_id}  

Get all users available
    [Arguments]     ${HEAD1}
    ${response}=        Get On Session     finvarPlatform        /api/v1/auth/       headers=${HEAD1}
    Status Should Be    200                 ${response}    

Get currently authenticated user information
    [Arguments]     ${HEAD1}
    ${response}=        Get On Session     finvarPlatform        /api/v1/auth/me      headers=${HEAD1}
    Status Should Be    200                 ${response}    