*** Settings ***
Documentation     Currencies API available on the Finvar Platform
Library     RequestsLibrary

*** Keywords ***
Get all currencies
    [Documentation]     Auth Apis integration test
    ${response}=        Get On Session     finvarPlatform        /api/v1/currencies       headers=${header2}
    Status Should Be    200                 ${response}    