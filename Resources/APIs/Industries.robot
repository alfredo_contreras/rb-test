*** Settings ***
Documentation     Countries API available on the Finvar Platform
Library     RequestsLibrary

*** Keywords ***
Get all industries
    [Documentation]     Industry Apis integration test
    ${response}=        Get On Session     finvarPlatform        /api/v1/industries       headers=${header2}
    Status Should Be    200                 ${response}    
    