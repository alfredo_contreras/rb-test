*** Settings ***
Documentation  Suite for Finvar Platform
#Library           DataDriver    file=RENTALO_MX.xlsx    sheet_name=CREARCUENTAS
Library     SeleniumLibrary

Resource  ./Resources/Common.robot  # necessary for Setup & Teardown
Resource  ./Resources/FinvarApp.robot
#Resource  ./Resources/PO/LandingPage.robot
Test Setup  Begin Web Test
Test Teardown  End Web Test


*** Variables ***
${BROWSER} =  chrome
${START_URL} =  https://web-qa.finvar.net/
${HOMEPAGE_TITLE} =  Finvar


# Copy/paste the below line to Terminal window to execute
# robot -d results tests/Rentalo.robot

*** Test Cases ***
Logged out user can review the Finvar website
#Finvar Platform load  
    [tags]  Landing Page    Smoke
    Given Finvar Platform website
    When User load the URL 
    Then the Finvar Platform website loaded 

Finvar Platform website have a General section  
    [tags]  Landing Page    Smoke
    Given Finvar Platform website
    When User load the URL 
    Then the Finvar Platform website loaded 
    and Then the Finvar Platform website have a General section


Finvar Platform website have a Features section  
    [tags]  Landing Page    Smoke
    Given Finvar Platform website
    When User load the URL 
    Then the Finvar Platform website have a Features section

Finvar Platform website have a Data & Analysis Features section  
    [tags]  Landing Page    Smoke
    Given Finvar Platform website
    When User load the URL 
    Then the Finvar Platform website have a Features section
    and Then the Finvar Platform website have a Data & Analysis Features section

*** Keywords ***
Finvar Platform website
    FinvarApp.Search for features
    #User click on Features nav menu

User load the URL
    Title Should Be    ${HOMEPAGE_TITLE}

Then the Finvar Platform website loaded  
    LandingPage.Verify Website loaded

Then the Finvar Platform website have a General section
    LandingPage.Verify General Section

the Finvar Platform website have a Features section
    LandingPage.Verify Features Menu available
    LandingPage.Goto Portfolio Features section

the Finvar Platform website have a Data & Analysis Features section
    LandingPage.Verify Features Menu available
    LandingPage.Goto Data & Analysis Features section

User click load the URL
    Click Button      //*[@id="root"]/div/div/div[1]/div[1]/div/nav/div/button


    